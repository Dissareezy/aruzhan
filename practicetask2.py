# Practice task 2
# Write a program that prompts the user to input a value of the variable x. Then the program should calculate and
# print the value of y according to the function:
#  y = 4x^2- 4x + 2
# Input:
# 5
# Output:
# 82
x = int(input("Please , enter the value of x"))

y = 4 * x * x - 4 * x + 2

print(y)
